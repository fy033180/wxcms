<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/12/13
  Time: 下午9:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <% String path = request.getContextPath(); %>
    <%--<link rel="stylesheet" href="//res.layui.com/layui/dist/css/layui.css"  media="all">--%>
    <link rel="stylesheet" href="<%=path%>/static/plugins/layui/css/layui.css"  media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>

<%--查询条件以及操作按钮--%>
<br>
<div class="layui-form">
    <%--查询条件--%>
        <div class="layui-form-item layui-form-pane">
            <%--<label class="layui-form-label">查询条件</label>--%>
            <%--<div class="layui-input-inline">--%>
                <%--<input type="text" name="title" required  lay-verify="required" placeholder="请输入菜单名" autocomplete="off" class="layui-input" id="channel_name">--%>
            <%--</div>--%>
            <%--<shiro:hasPermission name="channel:search">--%>
                <%--<button class="layui-btn"  id="search">--%>
                    <%--<i class="layui-icon">&#xe615;</i>搜索--%>
                <%--</button>--%>
            <%--</shiro:hasPermission>--%>
            <shiro:hasPermission name="channel:add">
                <button class="layui-btn" id="add" onclick="add()">
                    <i class="layui-icon">&#xe608;</i>添加
                </button>
            </shiro:hasPermission>
        </div>
</div>


<%--分页数据列表--%>
<div id="demo">
</div>


<script src="<%=path%>/static/plugins/layui/layui.js"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>

    var layout = [
        { name: '菜单名称', treeNodes: true, headerClass: 'value_col', colClass: 'value_col', style: 'width: 10%' },
        { name: '菜单ID',headerClass: 'value_col', colClass: 'value_col', style: 'width: 5%',
            render: function(row) {
                return '<div class="layui-table-cell laytable-cell-1-username">'+(typeof(row.id)=="undefined"?'':row.id)+'</div>'; //列渲染
            }
        },
        { name: '父菜单ID',headerClass: 'value_col', colClass: 'value_col', style: 'width: 5%',
            render: function(row) {
                return '<div class="layui-table-cell laytable-cell-1-username">'+(typeof(row.channel_parent)=="undefined"?'':row.channel_parent)+'</div>'; //列渲染
            }
        },
        { name: '栏目目录',headerClass: 'value_col', colClass: 'value_col', style: 'width: 20%',
            render: function(row) {
                return '<div class="layui-table-cell laytable-cell-1-username">'+(typeof(row.channel_catalog)=="undefined"?'':row.channel_catalog)+'</div>'; //列渲染
            }
        },
        { name: '内容页模板',headerClass: 'value_col', colClass: 'value_col', style: 'width: 20%',
            render: function(row) {
                return '<div class="layui-table-cell laytable-cell-1-username"><i class="layui-icon">'+(typeof(row.channel_page_template)=="undefined"?'':row.channel_page_template)+'</i></div>'; //列渲染
            }
        },
        { name: '索引页模板',headerClass: 'value_col', colClass: 'value_col', style: 'width: 20%',
            render: function(row) {
                return '<div class="layui-table-cell laytable-cell-1-username">'+(typeof(row.channel_index_template)=="undefined"?'':row.channel_index_template)+'</div>'; //列渲染
            }
        },

        {
            name: '操作',
            headerClass: 'value_col',
            colClass: 'value_col',
            style: 'width: 20%',
            render: function(row) {
                var chil_len=row.children.length;//获取每一级节点的长度
                var str ='';
                    str += '<a class="layui-btn layui-btn-primary layui-btn-xs" onclick="edit(\'' + row.id + '\')"><i class="layui-icon">&#xe615;</i> 查看</a>';
                <shiro:hasPermission name="channel:edit">
                    str += '<a class="layui-btn layui-btn-xs  layui-btn-normal" onclick="edit(\'' + row.id + '\')"><i class="layui-icon">&#xe642;</i> 编辑</a>'; //列渲染
                </shiro:hasPermission>

                if(chil_len==0){//判断是否有子节点
                <shiro:hasPermission name="channel:delete">
                    str+='<a class="layui-btn layui-btn-danger layui-btn-xs" onclick="del(\'' + row.id + '\')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                </shiro:hasPermission>
                }
                return str;
            }
        }
    ];


    layui.use(['tree','layer'], function(){
        var $ = layui.jquery;
        var layer = layui.layer;



    //添加数据
    add =function () {

      var index=top.layui.layer.open({
          title: '添加',
          maxmin: true,
          type: 2,
          content: '<%=path%>/views/cms/channel/edit.jsp',
          area: ['500px', '550px'],
    //                offset: 0,
          scrollbar: false,
    //                zIndex: layer.zIndex, //重点1
          success: function(layero, index) {
    //                    layer.setTop(layero); //重点2
    //                    layer.iframeAuto(index);
          },
          end: function () {
              inittree();//刷新
          }
      });
    };

    //编辑操作
    edit = function (id){
        if (id !=null && id != '') {
    //                var json = eval(data+']');
            top.layui.layer.open({
                title: '编辑',
                maxmin: true,
                type: 2,
                content: '<%=path%>/views/cms/channel/edit.jsp',
                area: ['500px', '550px'],
                scrollbar: false,
                success: function (layero, index) {
                    var body = top.layer.getChildFrame('body', index); //巧妙的地方在这里哦
//                    body.contents().find("#channel_pk").val(data.id);
                    debugger;
                    body.find("#channel_pk").val(id);
                },
                end: function () {
                    inittree();//刷新
                }
            });
        }else
        {
            layer.alert("请选择需要编辑的数据")
        }
    };


        //删除记录
        del = function (data) {
            var flag = false;
            layer.confirm("确认删除此数据吗？", {icon: 3, title: '提示'},
                function (index) {      //确认后执行的操作
                    $.ajax({
                        type: 'post',
                        url: '${basePath}/channel/delete',
                        data: {channel_pk: data},
                        success: function (response) {
                            if (response.state == "success") {
                                layer.close(index);
                                parent.layer.msg("删除成功");
                                inittree();
                            } else if (response.state == "fail") {
                                parent.layer.alert(response.msg);
                            }
                        },
                        error: function (response) {
                            layer.close(index);
                            parent.layer.alert(response.msg);
                        }
                    });
                },
                function (index) {      //取消后执行的操作
                    flag = false;
                });
        }

    //动态加载树形grid实例
    inittree = function () {
        $.ajax({
            type: "post",
            url: '<%=path%>/channel/channellist',
            dataType: 'json',
            success: function (response) {
                $("#demo").html("");
                //使用该树形菜单时必须有name,id两个字段，否则菜单会乱
                layui.treeGird({
                    elem: '#demo',
                    nodes: response,
                    layout:layout,
                    click: function (node, a) {
                        //console.log(a);
                    },
                    success: function () {

                    }
                });
            }
        })
    }

        $().ready(inittree());

    });
</script>


</body>
</html>

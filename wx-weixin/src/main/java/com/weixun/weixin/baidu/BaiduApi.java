package com.weixun.weixin.baidu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.weixin.sdk.msg.out.News;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BaiduApi {
	private static String apikey="90808a6f1c549c9f5d058bf443a47743";
	/**
	 * @param urlAll
	 *            :请求接口
	 * @param httpArg
	 *            :参数
	 * @return 返回结果
	 */
	public static String request(String httpUrl, String httpArg) {
	    BufferedReader reader = null;
	    String result = null;
	    StringBuffer sbf = new StringBuffer();
	    httpUrl = httpUrl + "?" + httpArg;

	    try {
	        URL url = new URL(httpUrl);
	        HttpURLConnection connection = (HttpURLConnection) url
	                .openConnection();
	        connection.setRequestMethod("GET");
	        // 填入apikey到HTTP header
	        connection.setRequestProperty("apikey",  apikey);
	        connection.connect();
	        InputStream is = connection.getInputStream();
	        reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	        String strRead = null;
	        while ((strRead = reader.readLine()) != null) {
	            sbf.append(strRead);
	            sbf.append("\r\n");
	        }
	        reader.close();
	        result = sbf.toString();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
	/**
	 * ip地址查询
	 * @param ip
	 * @return
	 */
	public static String queryIP(String ip){
		StringBuffer buffer = new StringBuffer();
		buffer.append("ip查询结果").append("\n\n");
		String httpUrl = "http://apis.baidu.com/apistore/iplookupservice/iplookup";
		String httpArg = "ip="+ip;
		String jsonResult = request(httpUrl, httpArg);
		JSONObject json = JSON.parseObject(jsonResult);
		Integer errNum=(Integer) json.get("errNum");
		if(errNum==0){
			Map map=(Map) json.get("retData");
			System.out.println(map);
			buffer.append("ip地址:").append(map.get("ip")).append("\n");
			buffer.append("网络:").append(map.get("carrier")).append("\n");
			buffer.append("国家:").append(map.get("country")).append("\n");
			buffer.append("省份:").append(map.get("province")).append("\n");
			buffer.append("市区:").append(map.get("city")).append("\n");
			buffer.append("地区:").append(map.get("district")).append("\n");
		}else{
			buffer.append("查询错误").append("\n");
		}
		return buffer.toString();
	}
	
	/**
	 * 身份证查询
	 * @param idnum
	 * @return
	 */
	public static String queryIdcard(String id){
		StringBuffer buffer = new StringBuffer();
		buffer.append("身份证查询结果").append("\n\n");
		String httpUrl = "http://apis.baidu.com/apistore/idservice/id";
		String httpArg = "id="+id;
		String jsonResult = request(httpUrl, httpArg);
		JSONObject json = JSON.parseObject(jsonResult);
		Integer errNum=(Integer) json.get("errNum");
		if(errNum==0){
			Map map=(Map) json.get("retData");
			System.out.println(map);
			buffer.append("性别:").append(map.get("sex")).append("\n");
			buffer.append("生日:").append(map.get("birthday")).append("\n");
			buffer.append("地址:").append(map.get("address")).append("\n");
		}else{
			buffer.append("查询错误").append("\n");
		}
		return buffer.toString();
	}
	
	/**
	 * 查询手机号码
	 * @param phone
	 * @return
	 */
	public static String queryPhone(String phone){
		StringBuffer buffer = new StringBuffer();
		buffer.append("手机号查询结果").append("\n\n");
		String httpUrl = "http://apis.baidu.com/apistore/mobilenumber/mobilenumber";
		String httpArg = "phone="+phone;
		String jsonResult = request(httpUrl, httpArg);
		JSONObject json = JSON.parseObject(jsonResult);
		Integer errNum=(Integer) json.get("errNum");
		if(errNum==0){
			Map map=(Map) json.get("retData");
			System.out.println(map);
			buffer.append("提供商:").append(map.get("supplier")).append("\n");
			buffer.append("省份:").append(map.get("province")).append("\n");
			buffer.append("城市:").append(map.get("city")).append("\n");
			buffer.append("卡类型:").append(map.get("suit")).append("\n");
			buffer.append("号码前七位:").append(map.get("prefix")).append("\n");
		}else{
			buffer.append("查询错误").append("\n");
		}
		return buffer.toString();
	}
	
	public static List<News> queryMM(int num){
		List<News> newsList=new ArrayList<News>();
		String httpUrl = "http://apis.baidu.com/txapi/mvtp/meinv";
		String httpArg = "num="+num;
		String jsonResult = request(httpUrl, httpArg);
		JSONObject json = JSON.parseObject(jsonResult);
		List list=(List) json.get("newslist");
		for(Object obj:list){
			Map map=(Map) obj;
			News news=new News(map.get("title").toString(), map.get("description").toString(), map.get("picUrl").toString(), map.get("url").toString());
			newsList.add(news);
		}
		return newsList;
	}
	
	public static String translate(String query){
		StringBuffer buffer = new StringBuffer();
		buffer.append("中英翻译结果").append("\n\n");
		String httpUrl = "http://apis.baidu.com/apistore/tranlateservice/translate";
		String httpArg = "query="+query+"&from=en&to=zh";
		String jsonResult = request(httpUrl, httpArg);
		JSONObject json = JSON.parseObject(jsonResult);
		Integer errNum=(Integer) json.get("errNum");
		if(errNum==0){
			Map map=(Map) json.get("retData");
			List list=(List) map.get("trans_result");
			Map result=(Map) list.get(0);
			buffer.append("原文:").append(result.get("src")).append("\n");
			buffer.append("译文:").append(result.get("dst")).append("\n");
		}else{
			buffer.append("翻译错误").append("\n");
		}
		return buffer.toString();
	}
	public static void main(String[] args) {
		
//		String httpUrl = "http://apis.baidu.com/apistore/mobilenumber/mobilenumber";
//		String httpArg = "phone=15210011578";
//		String jsonResult = request(httpUrl, httpArg);
//		System.out.println(BaiduApi.queryPhone("13697180404"));
//		//微信精选
//		String httpUrl = "http://apis.baidu.com/txapi/weixin/wxhot";
//		String httpArg = "num=10&rand=1&word=%E7%9B%97%E5%A2%93%E7%AC%94%E8%AE%B0&page=1";
//		String jsonResult = request(httpUrl, httpArg);
//		System.out.println(jsonResult);
//		String httpUrl = "http://apis.baidu.com/txapi/mvtp/meinv";
//		String httpArg = "num=10";
//		String jsonResult = request(httpUrl, httpArg);
//		System.out.println(jsonResult);
		//System.out.println(BaiduApi.queryIP("47.153.191.25511"));
//		String httpUrl = "http://apis.baidu.com/txapi/mvtp/meinv";
//		String httpArg = "num=5";
//		String jsonResult = request(httpUrl, httpArg);
//		JSONObject json = JSON.parseObject(jsonResult);
//		List list=(List) json.get("newslist");
//		System.out.println(list.get(0));
//		System.out.println(jsonResult);

		System.out.println(BaiduApi.translate("你好"));
	}

}
